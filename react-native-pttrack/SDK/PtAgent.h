//
//  PtAgent.h
//  PtAgent Version V1.2.4
//
//  Created by MiaoGuangfa on 16/6/3.
//  Copyright © 2016年 MiaoGuangfa. All rights reserved.
//

#import <Foundation/Foundation.h>


NS_ASSUME_NONNULL_BEGIN

@interface PtAgent : NSObject

+ (nonnull PtAgent *)sharedInstance;

/**
 * Start PTTrack engine with appKey(required) and optinal channel value
 * @param appKey, you can get it as soon as registered an application in our web management backend.
 * @param channel, the name of application store that you published your app, for iOS, the default value is : "AppStore".
 * You can pass nil or empty string, then the default value still will be used.
 *
 */
- (void)PTStartWithAppKey:(NSString *)appKey channel:(nullable NSString *)channel;

/**
 * If you want to collect app events by yourself, you can invoke this API
 * @param eventName, this value can't be nil or empty
 * @param properties, you can bind some event properties for every event, if you don't need it, just pass nil.
 *
 ************************************************************************************************************************************
 * [[PtAgent sharedInstance]PTTrackEvent:@"Buy" withProperties:@{@"price":@"200.00", @"title":@"xxx", ...}];
 ************************************************************************************************************************************
 *
 */
- (void)PTTrackEvent:(NSString *)eventName withProperties:(nullable NSDictionary *)properties;


/**
 * You can invoke this interface to collect app event without properties.
 */
- (void)PTTrackEvent:(NSString *)eventName;

/**
 * You can invoke this interface to mark the begin time for defined evnet.
 */
- (void)PTBeginEvent:(NSString *)eventName;

/**
 * Mark the end time for defined event, and send event pack to server.
 * And you can specific the event properties for yourself.
 */
- (void)PTEndEvent:(NSString *)eventName withProperties:(nullable NSDictionary *)properties;

/**
 * You can invoke this method to collect user login data
 */
- (void)PTTrackUserLogin:(NSString *)userID;

/**
 * Pack your defined error info
 * @param exceptionInfo error info
 * @param code          error code
 */
- (void)PTTrackException:(nullable NSString *)exceptionInfo exceptionCode:(NSInteger)code;

/**
 * You can specify an new page name to collect a PV package for the dynamic page view or the other scene.
 * @param pageName page name, this value can't be empty and nil
 */
- (void)PTPackVirtualPageViewWithName:(NSString *)pageName;

/**
 * If you want to set the specific pageName for current UIViewController by yourself,
 * you should set an new value as soon as -viewDidAppear is triggered(This point is very important, otherwise it will be invalid).
 * Defalut value is the current class name of UIViewController
 * @param pageName defined page name
 */
- (void)PTSetPageName:(nullable NSString *)pageName;

/**
 *  Set all PT defined log output disabled, default is open
 */
- (void)PTDisableLogOutput;

/**
 *
 * We strongly recommend that you should invoke this method to disable the graphical event feature before releasing your App version.
 * Default this feature is enabled.
 *
 */
- (void)PTDisableGraphicalEvent;
@end

NS_ASSUME_NONNULL_END
