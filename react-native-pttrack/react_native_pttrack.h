//
//  react_native_pttrack.h
//  react-native-pttrack
//
//  Created by ptmind on 11/01/2018.
//  Copyright © 2018 ptmind. All rights reserved.
//
#import <React/RCTBridgeModule.h>

@interface react_native_pttrack : NSObject <RCTBridgeModule>

@end
