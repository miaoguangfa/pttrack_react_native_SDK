/**
 * Stub of PtTrack for Android.
 *
 * @providesModule PtTrack
 * @flow
 */
'use strict';

var warning = require('fbjs/lib/warning');

var PtTrack = {
  test: function() {
    warning('Not yet implemented for Android.');
  }
};

module.exports = PtTrack;
