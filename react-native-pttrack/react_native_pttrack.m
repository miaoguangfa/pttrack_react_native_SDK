//
//  react_native_pttrack.m
//  react-native-pttrack
//
//  Created by ptmind on 11/01/2018.
//  Copyright © 2018 ptmind. All rights reserved.
//

#import "react_native_pttrack.h"
#import "PtAgent.h"

@implementation react_native_pttrack
RCT_EXPORT_MODULE()

RCT_EXPORT_METHOD(PTStartWithAppKey:(NSString *)appKey channel:(nullable NSString *)channel)
{
  // Your implementation here
  [[PtAgent sharedInstance]PTStartWithAppKey:appKey channel:channel];
}

RCT_EXPORT_METHOD(PTTrackEvent:(NSString *)eventName withProperties:(nullable NSDictionary *)properties)
{
  [[PtAgent sharedInstance]PTTrackEvent:eventName withProperties:properties];
}

RCT_EXPORT_METHOD(PTBeginEvent:(NSString *)eventName)
{
  [[PtAgent sharedInstance]PTBeginEvent:eventName];
}

RCT_EXPORT_METHOD(PTEndEvent:(NSString *)eventName withProperties:(nullable NSDictionary *)properties)
{
  [[PtAgent sharedInstance]PTEndEvent:eventName withProperties:properties];
}

RCT_EXPORT_METHOD(PTTrackUserLogin:(NSString *)userID)
{
  [[PtAgent sharedInstance]PTTrackUserLogin:userID];
}

RCT_EXPORT_METHOD(PTTrackException:(nullable NSString *)exceptionInfo exceptionCode:(NSInteger)code)
{
  [[PtAgent sharedInstance]PTTrackException:exceptionInfo exceptionCode:code];
}

RCT_EXPORT_METHOD(PTPackVirtualPageViewWithName:(NSString *)pageName)
{
  [[PtAgent sharedInstance]PTPackVirtualPageViewWithName:pageName];
}

RCT_EXPORT_METHOD(PTSetPageName:(nullable NSString *)pageName)
{
  [[PtAgent sharedInstance]PTSetPageName:pageName];
}

RCT_EXPORT_METHOD(PTDisableLogOutput)
{
  [[PtAgent sharedInstance]PTDisableLogOutput];
}

RCT_EXPORT_METHOD(PTDisableGraphicalEvent)
{
  [[PtAgent sharedInstance]PTDisableGraphicalEvent];
}
@end
