/**
 * @providesModule react-native-pttrack
 * @flow
 */
'use strict';

var NativePtTrack = require('NativeModules').react-native-pttrack;

/**
 * High-level docs for the react-native-pttrack iOS API can be written here.
 */

var react-native-pttrack = {
  // test: function() {
  //   NativePtTrack.test();
  // }

};

module.exports = react-native-pttrack;
